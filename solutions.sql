 --Matthew A. Manalili CSIT343 - G2
 Create SQL Queries using advanced selects and joining tables to return the following:
 
 a. Find all artists that has letter D in its name.
    Syntax
        SELECT * FROM artists WHERE name LIKE "%D%";

 b. Find all songs that has a length of less than 230.
    Syntax
        SELECT * FROM songs WHERE length < 230;

 c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
    Syntax
        SELECT album_title,song_name,length FROM albums JOIN songs ON albums.id = songs.album_id;

 d. Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
    Syntax
        SELECT * FROM artists JOIN albums on artists.id = albums.artist_id WHERE albums.album_title LIKE "%A%";

 e. Sort the albums in Z-A order. (Show only the first 4 records.)
    Syntax
        SELECT * FROM albums ORDER BY album_title desc LIMIT 4;

 f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
    Syntax
        SELECT * FROM albums JOIN songs on albums.id = songs.album_id ORDER BY albums.album_title DESC, songs.song_name ASC;

